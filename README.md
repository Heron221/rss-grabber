Install requirements.txt and then follow these commands to run the project:



#django

python manage.py makemigrations

python manage.py migrate

python manage.py runserver



#celery-beat

celery -A rses beat -l INFO



#celery-worker

on windows --> celery -A rses worker --pool=solo -l INFO

on linux --> celery -A rses worker -l INFO



Also, there is a postman collection file that you can use to test the APIs.
