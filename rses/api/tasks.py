from celery import Celery
from .models import News
import requests
import xml.etree.ElementTree as ET


app = Celery('rses')


@app.task(bind=True)
def grab_rss(self):

    url = 'https://www.farsnews.ir/rss'
    response = requests.get(url)

    with open('RSSes.xml', 'wb') as f:
        f.write(response.content)

    tree = ET.parse('RSSes.xml')
    root = tree.getroot()

    for item in root.findall('./channel/item/title'):
        title = item.text

        try:
            News.objects.create(title=title)
        except Exception as e:
            print(e)

