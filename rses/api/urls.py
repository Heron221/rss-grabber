from django.urls import path
from .views import RssListView

urlpatterns = [
    path('rss/', RssListView.as_view()),
]
