from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from .models import News
from .serializers import NewsSerializer


class RssListView(ListAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = [IsAuthenticated]

