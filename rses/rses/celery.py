from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rses.settings')
app = Celery('rses')
app.conf.enable_utc = False

app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.broker_url = os.environ.get('REDIS_URL', 'redis://localhost:6379')
app.conf.beat_scheduler = 'django_celery_beat.schedulers.DatabaseScheduler'

app.conf.beat_schedule = {
    'grab_rss': {
        'task': 'api.tasks.grab_rss',
        'schedule': crontab(hour='*/1')
    },
}
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
